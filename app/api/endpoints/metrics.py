from functools import lru_cache

from fastapi import APIRouter, Request
from fastapi.responses import JSONResponse

from api.impl.filters import filters
from api.impl.log import get_data_by_params
from api.models import ClientMetricsResponse

router = APIRouter()


@lru_cache(maxsize=32)
@router.get("/user", response_model=ClientMetricsResponse)
async def get_by_user(username: str):
    username_filter: filters.UsernameFilter = filters.UsernameFilter(username)
    username_metrics = username_filter.execute("metrics")
    if username_metrics == {}:
        return JSONResponse(content={}, status_code=404)
    return ClientMetricsResponse(**username_metrics)


@lru_cache(maxsize=32)
@router.get("/{path_param}")
async def get_by_params(path_param: str, request: Request) -> JSONResponse:
    """Returns data based on path param and filters by query based on value.

    Args:
        path_param (str): action to execute.
        request (Request): HTTP Request.

    Returns:
        [JSONResponse]: returns data and HTTP code based on execute method.
    """
    query_params: dict = dict(request.query_params)
    data, status_code = await get_data_by_params(query_params, path_param)
    return JSONResponse(content=data, status_code=status_code)
