from fastapi import APIRouter

from api.impl.log import create_log
from api.models import Log, LogResponse


router = APIRouter()


@router.post("/log", response_model=LogResponse, status_code=201)
async def post(log: Log) -> LogResponse:
    """Call create_log ,ethod to store log.

    Args:
        log (Log): HTTP body.

    Returns:
        [LogResponse]: log stored formatted as LogResponse.
    """
    log_created = await create_log(log.raw_log)
    return LogResponse(**log_created)
