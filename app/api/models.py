import datetime
import ipaddress

from pydantic import BaseModel

EXPECTED_KEYS: list = [
    "username", "client", "ip_host", "ip_internal", "timestamp", "status"]


class Log(BaseModel):
    raw_log: str


class LogResponse(BaseModel):
    id: str
    username: str
    client: str
    ip_host: ipaddress.IPv4Address
    ip_internal: bool
    timestamp: datetime.datetime
    status: int


class ClientMetricsResponse(BaseModel):
    clients: list
    ip_hosts: list
    invalid_count: list
