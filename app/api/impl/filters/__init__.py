from abc import ABC, abstractmethod
import inspect
from typing import Tuple

from fastapi import HTTPException

from api.db import database
from api.models import EXPECTED_KEYS


class Filter(ABC):

    def __init__(self, value: str, query: str):
        """ Constructuor method.

        Args:
            query (str): query type.
            value (str): Vlue to query.

        Raises:
            HTTPException: raises HTTP exception if query can not be executed.
        """
        if query not in EXPECTED_KEYS:
            raise HTTPException(
                status_code=400,
                detail=f"Is not posible to query by {query}"
            )

        self.value = value
        self.query = query

    def is_known(self) -> Tuple[dict, int]:
        """Looks up if there are log base on query and value.

        Returns:
            Tuple[dict, int]: returns if there are logs based on
            query and value, http value.
        """
        for _, log in database.items():
            log_ip = log.get(self.query)
            if log_ip == self.value:
                return {
                    self.query: self.value,
                    "is_known": True
                }, 200
        return {
            self.query: self.value,
            "is_know": False
        }, 400

    def invalid_request_count(self) -> Tuple[dict, int]:
        """Returns quantity of logs with invalid HTTP codes.

        Returns:
            Tuple[dict, int]: contains quantity of
            logs with invalid HTTP codes, status code
        """
        quantity = len(list(
            filter(lambda log: self._is_invalid_log(log), database.values())
        ))
        return {
            self.query: self.value,
            "quantity": quantity
        }, 200

    def _is_invalid_log(self, log: dict) -> bool:
        """Returns true if log has invalid http code, otherwise returns False.

        Args:
            log (dict): log stored in database.

        Returns:
            bool: returns True if status is invalid, otherwise False.
        """
        status = str(log.get("status"))
        return len(status) == 3 and not status.startswith("2")

    @classmethod
    def valid_filters(cls) -> list:
        """Return function names by class.

        Returns:
            [list]: list of functions by class.
        """
        return [func[0] for func in inspect.getmembers(
            cls, predicate=inspect.isfunction)]

    @abstractmethod
    def execute(self, action: str):
        pass
