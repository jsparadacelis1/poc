from typing import Tuple

from fastapi import HTTPException

from api.db import database
from api.impl.filters import Filter


class UsernameFilter(Filter):

    def __init__(self, value: str, query: str = "username"):
        super().__init__(value, query)

    def metrics(self) -> dict:
        metrics_by_username: dict = {
            "clients": [],
            "ip_hosts": [],
            "invalid_count": 0
        }
        logs_by_username = list(
            filter(
                lambda log: log.get("username") == self.value,
                database.values())
        )

        if not logs_by_username:
            return {}

        for log in logs_by_username:
            metrics_by_username["ip_hosts"].append({
                "ip_host": log.get("ip_host"),
                "is_internal": log.get("ip_internal")
            })
            metrics_by_username["clients"].append(log.get("client"))

        metrics_by_username["invalid_count"] = self.invalid_request_count()
        return metrics_by_username

    def execute(self, action: str):
        """Executes an action based on action value.

        Args:
            action (str): action to execute.

        Raises:
            HTTPException: if action can not be axecuted,
            raises 400 http code exception.

        Returns:
            [Tuple]: data, http code.
        """
        if action not in self.valid_filters():
            raise HTTPException(
                status_code=400,
                detail=f"Is not possible to get {action}"
            )

        action_to_execute = {
            "is_known": self.is_known(),
            "invalid_request_count": self.invalid_request_count(),
            "metrics": self.metrics()
        }
        return action_to_execute.get(action)


class IpFilter(Filter):

    def __init__(self, value: str, query: str = "ip_host"):
        super().__init__(value, query)

    def execute(self, action: str):
        """Executes an action based on action value.

        Args:
            action (str): action to execute.

        Raises:
            HTTPException: if action can not be axecuted,
            raises 400 http code exception.

        Returns:
            [Tuple]: data, http code.
        """
        if action not in self.valid_filters():
            raise HTTPException(
                status_code=400,
                detail=f"Is not possible to get {action}"
            )

        action_to_execute = {
            "is_known": self.is_known(),
            "invalid_request_count": self.invalid_request_count()
        }
        return action_to_execute.get(action)


class ClientFilter(Filter):

    def __init__(self, value: str, query: str = "client"):
        super().__init__(value, query)

    def quantity(self) -> Tuple[dict, int]:
        """Returns quantity of logs with value as the cleint.

        Returns:
            [tuple]: returns quantity of logs that contains
            client as value, http code.
        """
        quantity = len(list(
            filter(
                lambda log: log.get(self.query) == self.value,
                database.values()
            )
        ))
        return {
            self.query: self.value,
            "quantity": quantity
        }, 200

    def execute(self, action: str):
        """Executes an action based on action value.

        Args:
            action (str): action to execute.

        Raises:
            HTTPException: if action can not be axecuted,
            raises 400 http code exception.

        Returns:
            [Tuple]: data, http code.
        """
        if action not in self.valid_filters():
            raise HTTPException(
                status_code=400,
                detail=f"Is not possible to get {action}"
            )

        action_to_execute = {
            "is_known": self.is_known(),
            "invalid_request_count": self.invalid_request_count(),
            "quantity": self.quantity()
        }
        return action_to_execute.get(action)
