from datetime import datetime
from uuid import uuid1
from typing import Tuple

from fastapi import HTTPException

from api.db import database
from api.impl.filters import filters


async def create_log(raw_log: str) -> dict:
    """Creates a log in database dict.

    Args:
        raw_log (str): raw log to extract values and store the log.

    Returns:
        [dict]: returns dict stored in database.
    """
    user, client, ip_host, ip_internal, date, time, status = raw_log.split(" ")
    timestamp = datetime.strptime(f"{date}T{time}", "%Y/%m/%dT%H:%M:%S")
    is_internal = True if ip_internal == "true" else False
    log_to_save: dict = {
        "username": user,
        "client": client,
        "ip_host": ip_host,
        "ip_internal": is_internal,
        "timestamp": timestamp,
        "status": status
    }
    log_id = str(uuid1())
    if not database.get(log_id):
        database[log_id] = log_to_save
        log_to_save["id"] = log_id

    return log_to_save


async def get_data_by_params(query_params: dict, action: str) -> Tuple:
    """Returns JSONResponse with data and http code.

    Args:
        query_params (dict): http query with query and value to query.
        action (str): action to execute in query object by value.

    Raises:
        HTTPException: raises HTTP exception if filter type is not valid.

    Returns:
        [Tuple]: returns data and HTTP code based on execute method.
    """
    query, value = list(query_params.items())[0]
    filters_type: dict = {
        "username": filters.UsernameFilter,
        "ip_host": filters.IpFilter,
        "client": filters.ClientFilter
    }

    filter_type: filters.Filter = filters_type.get(query)
    if not filter_type:
        raise HTTPException(
            status_code=400,
            detail=f"Is not possible to filter by {query}"
        )

    filter_type = filter_type(value, query)
    return filter_type.execute(action)
