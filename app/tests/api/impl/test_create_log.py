import asyncio

from api.db import database
from api.impl.log import create_log
from api.models import LogResponse


def test_create_log():
    test_log = "tester mozilla 127.89.0.2 true 2014/06/16 05:42:52 200"
    user, client, ip_host, ip_internal, date, time, status =\
        test_log.split(" ")

    log_created = asyncio.run(create_log(test_log))
    log_created_id = log_created.get("id")
    log_saved = database.get(log_created_id)

    assert log_saved
    assert log_saved == log_created
    assert isinstance(LogResponse(**log_created), LogResponse)
