from datetime import datetime
from unittest import mock

from fastapi import HTTPException
import pytest

from api.impl.log import get_data_by_params


timestamp = datetime.now()
base_db_mock = {
    "123456-abcde-232a-123": {
        "username": "Tester1",
        "client": "MyDevice1",
        "ip_host": "125.0.2.35",
        "ip_internal": "true",
        "timestamp": timestamp,
        "status": "200"
    },
    "123456-abcde-232a-124": {
        "username": "Tester2",
        "client": "MyDevice2",
        "ip_host": "125.0.2.34",
        "ip_internal": "true",
        "timestamp": timestamp,
        "status": "200"
    }
}


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "username, action",
    (("Tester1", "is_known"), ("Tester2", "is_known"))
)
async def test_is_known_by_username(username: str, action: str):

    with mock.patch("api.impl.filters.database", base_db_mock):
        query_params: dict = {"username": username}
        response, status = await get_data_by_params(query_params, action)
        assert response
        assert status == 200
        assert response.get("is_known")


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "ip, action",
    (
        ("125.0.2.35", "invalid_request_count_test"),
        ("125.0.2.34", "invalid_request_count")
    )
)
async def test_invalid_request_count_by_username(ip: str, action: str):

    with mock.patch("api.impl.filters.database", base_db_mock):
        query_params: dict = {"ip_host": ip}

        try:
            response, status = await get_data_by_params(query_params, action)
            assert response
            assert status == 200
            assert not response.get("quantity")
        except Exception as e:
            assert isinstance(e, HTTPException)
            assert e.status_code == 400
