from fastapi import FastAPI

from api.endpoints import log, metrics


def create_app() -> FastAPI:
    """Creates Fast API app.

    Returns:
        FastAPI: Fast API app.
    """
    app = FastAPI()
    app.include_router(log.router, tags=["log"])
    app.include_router(metrics.router, prefix="/metrics", tags=["metrics"])
    return app


app = create_app()
