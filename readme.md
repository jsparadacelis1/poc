# Logs

This project allows to store a log in a stub database.

## Analysis

This project was implemented using FastAPI. A Python web framework that uses the asyncio library "under the hood" to improve response times over other frameworks such as Flask or Django. It also allows validation of the input and output schemas of HTTP requests through the Pydantic library which uses duck typing creating models similar to Python dataclasses, for these validations. This saves a lot of time in the construction of validation schemes compared to using libraries such as Marshmallow. 

For this test I concentrated on the design of the different filters that can be applied according to the http requests made. For this case, I propose the implementation of a factory method that at the same time can be seen as a strategy pattern since the functionality of each filter can be extended. In this case, the execute method of each filter makes the decision of which operation to perform.

![Abstract factory](uml.png)

Given that for now the logic of each filter is small, the concrete filters are inside the same file inside the filters folder. On the other hand, the abstract class Filter is inside the `__init__` file of the same folder. This can be seen in the project structure.

In addition, I would like to highlight the application of the `lru_cache` function of the python functools module. Given the scope of the test, I consider that this function works well. However, taking into account that this application must respond to several clients at the same time, and given that several users of those clients can be repeated, a cache can be implemented with this input data to improve the performance of this application, taking into account that the input and output services (POST, GET) work within the same service. In this case, the `lru_cache` implementation acts as a "mock" of a production implementation. 

However, the services can be separated so that the fetching service is not affected by the write times of the log storage service. 

## Project structure

```
app/
    api/
        endpoints/
            log
            metrics
        impl/
            filters/...
            log
        db
        models
    tests/
        api/
            impl/...
    Dockerfile
    requirements.txt
    main.py
docker-compose.yml
Makefile

```

## How to run

To run this project need to have installed Docker and docker-compose:

1. Build and run project
    - ``` docker-compose up --build ```
2. Stops containers and removes containers
    - ``` docker-compose down ```
3. Run tests
    - ``` docker-compose run web pytest ```

Also, you can use makefile commands:

1. Build
    - ``` make build ```
2. Up
    - ``` make up ```
3. Run tests
    - ``` make unit ```

## Log structure

``` <username> <client> <ip> <ip_internal?> <date> <time> <http-code> ```

## Usage

To use this project, you can execute http calls using [httpie](https://httpie.io/) client.

1. Store command.
    - ``` http POST http://0.0.0.0:8001/log raw_log="tester1 mozilla 127.89.0.2 true 2014/06/16 05:42:52 400" ```
    ![](get.png)

2. Get command.
    - ``` http http://0.0.0.0:8001/metrics/is_known?username=tester1 ```
    ![](post.png)
